from __future__ import print_function
from subprocess import call

import socket
import sys
import os

from message import Message

class Player(object):
    EMPTY_SPACE = ' '
    DESTROYED_SHIP = 'X'

    def __init__(self, num, address, next_num, next_addr):
        self.num = num
        self.addr = address         #('localhost', 10001)
        self.next_player = next_num
        self.next_addr = next_addr
        self.has_token = (num == 1);
        self.is_playing = 1
        self.boardSize = 5
        self.board = []
        for i in range(self.boardSize): self.board.insert(i, [])
        self.boardHorizontalZoom = 4
        self.boardVerticalZoom = self.boardHorizontalZoom / 3 + 1
        self.ship = []
        self.shipNum = 2
        self.history = []
        self.historySize = 3
        self.inGame = True
        self.playerInGame = [True, True, True, True]

    def shipWillBeDestroyed(self, i, j):
        # (i, j) are the positions where the attack occurred
        attackedShip = self.board[i][j]

        if i-1 >= 0 and self.board[i-1][j] == attackedShip: return False
        if i+1 < self.boardSize and self.board[i+1][j] == attackedShip: return False
        if j-1 >= 0 and self.board[i][j-1] == attackedShip: return False
        if j+1 < self.boardSize and self.board[i][j+1] == attackedShip: return False
        return True

    def getShipPositions(self, i, j):
        ship = int(self.board[i][j]);
        response = ''
        for i in range(len(self.ship[ship-1])):
            data = self.ship[ship-1][i].split(',')
            response += '({},{})'.format(int(data[0]) + 1, int(data[1]) + 1)
        return response;

    def clearHistory(self):
        self.history = []

    def setDefeat(self):
        self.inGame = False
        self.clearHistory()
        insertHistory(self, 'Voce foi derrotado!\n')
        printScreen(self)

    def won(self):
        for i in range(4):
            if i+1 != self.num:
                if self.playerInGame[i]: return False
        return True

    def finishGame(self):
        self.clearHistory()
        printScreen(self)
        raw_input('Voce ganhou!!1!onze1!\nPressione ENTER para sair...\n')
        quit()


def play(player_num, server_address, next_num, next_addr):

    player = Player(player_num, server_address, next_num, next_addr)
    message = Message()
    readBoard(player)

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the socket to the port
    insertHistory(player, 'Conectando em %s na porta %s\n' % server_address)
    sock.bind(server_address)


    while True:
        if player.inGame:
            if player.has_token:
                printScreen(player)
                print('Sua vez. Voce esta com o bastao!')

                try:
                    player_to_atack = raw_input('Qual jogador vc quer atacar? (1,2,3,4): ')
                    if (player_to_atack == str(player.num)):
                        insertHistory(player, 'ERRO: Nao pode atacar vc mesmo.\n')
                        continue
                    elif int(player_to_atack) <= 0 or int(player_to_atack) > 4:
                        insertHistory(player, 'ERRO: Jogador {} eh invalido!\n'.format(player_to_atack))
                        continue
                    else:
                        if not player.playerInGame[int(player_to_atack) - 1]:
                            insertHistory(player, 'ERRO: Este jogador ja foi derrotado!\n')
                            printScreen(player)
                            continue
                except Exception as e:
                    insertHistory(player, 'ERRO: Valor {} eh invalido!\n'.format(player_to_atack))
                    continue

                while True:
                    printScreen(player)
                    try:
                        print('Onde acontecera o ataque?')
                        y = raw_input('Coordenada Y (vertical): ')
                        x = raw_input('Coordenada X (horizontal): ')
                        y = int(y)
                        x = int(x)

                        if y <= 0 or y > 5 or x <= 0 or x > 5:
                            insertHistory(player, 'Coordenadas invalidas!\n')
                            continue

                        coordenates_to_atack = '(' + str(y) + ',' + str(x) + ')'
                        break
                    except Exception as e:
                        insertHistory(player, 'Coordenadas invalidas!\n')
                        continue

                data = 'MOVE:%s' % coordenates_to_atack
                buffer_msg = message.setAttackMsg(player.num, player_to_atack, data)
                sent = sock.sendto(buffer_msg, player.next_addr)

                sock.settimeout(2.0)

                try:
                    answer, addr = sock.recvfrom(4096)
                    message = Message()
                    message.transformToMessage(answer)
                    
                    if message.status == message.status_ack_destroyed: # Se o ataque destruiu o navio
                        insertHistory(player, 'Voce destruiu o navio do jogador {}!\nPosicoes: {}\n'.format(player_to_atack, message.data))
                        message = message.setNotification(player.num, message.status_notify_destroyed, player_to_atack + ":" + message.data)
                    elif message.status == message.status_ack_hit:
                        insertHistory(player, 'Voce acertou um navio do jogador {}!\n'.format(player_to_atack))
                        message = message.setNotification(player.num, message.status_notify_hit, player_to_atack)
                    elif message.status == message.status_ack_defeat:
                        insertHistory(player, 'Voce derrotou o jogador {}!\n'.format(player_to_atack))
                        player.playerInGame[int(player_to_atack) - 1] = False
                        message = message.setNotification(player.num, message.status_notify_defeat, player_to_atack)
                        if player.won():
                            player.finishGame()
                    else:
                        insertHistory(player, 'Voce errou!\n')
                        message = message.setNotification(player.num, message.status_notify_miss, player_to_atack)

                    printScreen(player)

                    sock.settimeout(None)
                    sent = sock.sendto(message, player.next_addr)

                    # Loop of resending notification
                    while True:
                        sock.settimeout(2.0)
                        try:
                            answer, addr = sock.recvfrom(4096)
                            break
                        except Exception as e:
                            insertHistory(player, 'Nao foi possivel notificar os jogadores. Tentando de novo.\n')
                            sock.settimeout(None)
                            sent = sock.sendto(message, player.next_addr)
                            continue
                        printScreen(player)

                except Exception as e:
                    sock.settimeout(None)
                    insertHistory(player, 'Nao foi possivel enviar o ataque. Tente novamente.\n')
                    continue

                sock.settimeout(None)
                printScreen(player)


                player.has_token = 0  #temporariamente
                token = Message()
                message = token.setTokenMsg(player.num, player.next_player)
                sent = sock.sendto(message, player.next_addr)

            else: # if player.has_token
                printScreen(player)
                print('Esperando sua vez.')
                buffer_msg, address = sock.recvfrom(4096)
                if buffer_msg:
                    message = Message()
                    message.transformToMessage(buffer_msg)

                    if message.isToMe(player.num) and not message.isNotification():
                        if message.isToken():
                            printScreen(player)
                            player.has_token = 1
                            continue
                        elif message.isAtack():
                            data = message.data
                            if message.isValid():
                                status = ''
                                attackedPos = message.attackPos()

                                insertHistory(player, 'Voce acabou de receber um ataque do jogador %s.\n' % message.sender)
                                insertHistory(player, 'Ataque ocorreu na posicao %s\n' % attackedPos)

                                attackedPos = attackedPos.translate(None, '()')
                                attackedPos = attackedPos.split(',')
                                i = int(attackedPos[0]) - 1
                                j = int(attackedPos[1]) - 1

                                if player.board[i][j] != player.DESTROYED_SHIP and player.board[i][j] != player.EMPTY_SPACE:
                                    if player.shipWillBeDestroyed(i, j):
                                        player.shipNum -= 1
                                        if player.shipNum == 0:
                                            status = message.status_ack_defeat
                                            player.setDefeat()
                                        else:
                                            status = message.status_ack_destroyed
                                            data = player.getShipPositions(i, j)
                                    else:
                                        status = message.status_ack_hit
                                    player.board[i][j] = player.DESTROYED_SHIP
                                else:
                                    status = message.status_ack_miss
                                
                                printScreen(player)
                                buffer_msg = message.setReceivedMsg(data, status)
                            sent = sock.sendto(buffer_msg, player.next_addr)
                        else:
                            insertHistory(player, 'ERRO: mensagem sem tipo\n')
                            printScreen(player)
                    elif not message.isToMe(player.num) and message.isNotification():
                        printScreen(player)

                        if message.status == message.status_notify_destroyed:
                            # insertHistory('Recebi ' + message.data + '\n')
                            data = message.data.split(':')
                            target = data[0]
                            positions = data[1]
                            insertHistory(player, 'Jogador {} destruiu um navio do jogador {}!\nPosicoes: {}\n'.format(message.sender, target, positions))
                        elif message.status == message.status_notify_hit:
                            insertHistory(player, 'Jogador {} acertou um navio do jogador {}!\n'.format(message.sender, message.data))
                        elif message.status == message.status_notify_defeat:
                            insertHistory(player, 'Jogador {} derrotou o jogador {}!\n'.format(message.sender, message.data))
                            player.playerInGame[int(message.data) - 1] = False
                        else:
                            insertHistory(player, 'Jogador {} atacou o jogador {} e errou!\n'.format(message.sender, message.data))

                        printScreen(player)
                        sent = sock.sendto(buffer_msg, player.next_addr)
                    else:
                        sent = sock.sendto(buffer_msg, player.next_addr)
        else: #if player.inGame
            buffer_msg, address = sock.recvfrom(4096)
            message = Message()
            message.transformToMessage(buffer_msg)
            if message.isToken():
                buffer_msg = message.setTokenMsg(player.num, player.next_player)
            sent = sock.sendto(buffer_msg, player.next_addr)
            


    print >>sys.stderr, 'closing socket'
    sock.close()

def drawBoard(player):

    print(' ', end='')
    for i in range(player.boardSize):
        for j in range(player.boardHorizontalZoom):
            if j == player.boardHorizontalZoom/2:
                print(i+1, end='')
            else:
                print(' ', end='')
    print(' ')

    print(' ', end='')
    for i in range(player.boardSize * player.boardHorizontalZoom + 2):
        print('-', end='')
    print('')

    for i in range(player.boardSize):
        for m in range(player.boardVerticalZoom):
            print('{}|'.format(i+1), end='') if m == player.boardVerticalZoom/2 else print(' |', end='')
            for j in range(5):
                for k in range(player.boardHorizontalZoom):
                    print(player.board[i][j], end='')
            print('|')

    print(' ', end='')
    for i in range(player.boardSize * player.boardHorizontalZoom + 2):
        print('-', end='')
    print('\n')

def printScreen(player):
    call(['clear'])
    print('JOGADOR {}\n'.format(player.num))
    drawBoard(player)
    for i in range(len(player.history)):
        print(player.history[i], end='')

def insertHistory(player, message):
    if len(player.history) < player.historySize:
        player.history.insert(len(player.history), message)
    else:
        for i in range(player.historySize - 1):
            player.history[i] = player.history[i+1]
        player.history[player.historySize - 1] = message

def readBoard(player):
    for i in range(player.boardSize):
        for j in range(player.boardSize):
            player.board[i].insert(j, ' ')

    for i in range(player.shipNum):
        while True:
            printScreen(player)

            try:
                coord = raw_input('Insira as coordenadas do navio {} ( i j ): '.format(i+1))
                coord = coord.split()
                if len(coord) != 2:
                    continue
                coord[0] = int(coord[0])
                coord[1] = int(coord[1])
            except Exception as e:
                continue

            if coord[0] <= 0 or coord[0] > player.boardSize or coord[1] <= 0 or coord[1] > player.boardSize:
                insertHistory(player, 'Coordenadas invalidas!\nPor favor, escolha coordenadas dentro do intervalo [{}...{}]\n'.format(1, player.boardSize))
                continue
            else:
                # Coordinates of ship
                cI = coord[0]-1
                cJ = coord[1]-1
                
                # Check all four directions
                up = checkUpwards(player, cI, cJ)
                down = checkDownwards(player, cI, cJ)
                left = checkLeft(player, cI, cJ)
                right = checkRight(player, cI, cJ)

                message = 'Selecione a direcao do navio ('
                
                if up: message += 'up'
                if down and up: message += ', down'
                if down and not up: message += 'down'
                if left and (down or up): message += ', left'
                if left and not (down or up): message += 'left'
                if right and (left or down or up): message += ', right'
                if right and not (left or down or up): message += 'right'
                message += '): '

                if not up and not down and not left and not right:
                    insertHistory(player, 'Ja existe uma navio nesta posicao!\n')
                    continue

                while True:
                    printScreen(player)

                    invalidDirection = False
                    exit = False

                    direction = raw_input(message)

                    if direction == 'up' and up: placeShip(player, i+1, cI, cJ, direction); exit = True
                    if direction == 'down' and down: placeShip(player, i+1, cI, cJ, direction); exit = True
                    if direction == 'left' and left: placeShip(player, i+1, cI, cJ, direction); exit = True
                    if direction == 'right' and right: placeShip(player, i+1, cI, cJ, direction); exit = True

                    if exit: break

                    if direction == 'up' and not up: invalidDirection = True
                    if direction == 'down' and not down: invalidDirection = True
                    if direction == 'left' and not left: invalidDirection = True
                    if direction == 'right' and not right: invalidDirection = True
                    if direction != 'up' and direction != 'down' and direction != 'left' and direction != 'right': invalidDirection = True

                    if invalidDirection:
                        insertHistory(player, 'Direcao invalida!\n')
                    else:
                        break

                player.board[coord[0]-1][coord[1]-1] = i+1
                break

    printScreen(player)

def checkUpwards(player, i, j):
    if i - 2 < 0: return False
    
    for k in range(i, i-3, -1):
        if player.board[k][j] != ' ': return False
    return True

def checkDownwards(player, i, j):
    if i + 2 >= player.boardSize: return False

    for k in range(i, i+3):
        if player.board[k][j] != ' ': return False
    return True

def checkRight(player, i, j):
    if j + 2 >= player.boardSize: return False

    for k in range(j, j+3):
        if player.board[i][k] != ' ': return False
    return True

def checkLeft(player, i, j):
    if j - 2 < 0: return False

    for k in range(j, j-3, -1):
        if player.board[i][k] != ' ': return False
    return True

def placeShip(player, ship, i, j, direction):
    positions = []
    
    if direction == 'up':
        for k in range(i, i-3, -1):
            player.board[k][j] = ship
            positions.insert(len(positions), '{},{}'.format(k, j))
        player.ship.insert(len(player.ship), positions);
        return True

    if direction == 'down':
        for k in range(i, i+3):
            player.board[k][j] = ship
            positions.insert(len(positions), '{},{}'.format(k, j))
        player.ship.insert(len(player.ship), positions);
        return True

    if direction == 'left':
        for k in range(j, j-3, -1):
            player.board[i][k] = ship
            positions.insert(len(positions), '{},{}'.format(i, k))
        player.ship.insert(len(player.ship), positions);
        return True

    if direction == 'right':
        for k in range(j, j+3):
            player.board[i][k] = ship
            positions.insert(len(positions), '{},{}'.format(i, k))
        player.ship.insert(len(player.ship), positions);
        return True

    return False