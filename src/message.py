class Message:
    access_control_not_token = 'A'
    access_control_is_token = 'B'
    
    msg_control_notification = 'C'
    msg_control_attack = 'D'
    
    status_sent = 'E'
    status_arrived = 'F'
    
    status_ack_hit = 'G'
    status_ack_miss = 'H'
    status_ack_destroyed = 'I'
    status_ack_defeat = 'M'
    
    status_notify_destroyed = 'J'
    status_notify_hit = 'K'
    status_notify_miss = 'L'
    status_notify_defeat = 'N'
    # status_ack = 'G'
    # status_received = 'H'


    def __init__(self):
        self.start = '~'
        self.access_control = 'A'
        self.msg_control = 'C'
        self.data = ''
        self.status = ''
        self.sender = ''
        self.receiver = ''

    def isToMe(self, current_player):
        return (str(self.receiver) == str(current_player))

    def isToken(self):
        return (str(self.access_control) == 'B')

    def isNotification(self):
        return (str(self.msg_control) == 'C')

    def isAtack(self):
        return (str(self.msg_control) == 'D')

    def transformToMessage(self, buffer_msg):
        self.start = buffer_msg[:1]
        self.access_control = buffer_msg[1:2]
        self.msg_control = buffer_msg[2:3]
        self.status = buffer_msg[3:4]
        self.sender = buffer_msg[4:5]
        self.receiver = buffer_msg[5:6]
        self.data = buffer_msg[6:]

    def isValid(self):
        return True

    def setNotification(self, sender, status, data):
        self.access_control = self.access_control_not_token
        self.msg_control = self.msg_control_notification
        self.status = status
        self.sender = sender
        self.receiver = 0
        self.data = data
        return str(self.start) + str(self.access_control) + str(self.msg_control) + str(self.status) + str(self.sender) + str(self.receiver) + str(self.data)

    def setAttackMsg(self, sender, receiver, data):
        self.access_control = self.access_control_not_token
        self.msg_control = self.msg_control_attack
        self.status = self.status_sent
        self.sender = sender
        self.receiver = receiver
        self.data = data
        return str(self.start) + str(self.access_control) + str(self.msg_control) + str(self.status) + str(self.sender) + str(self.receiver) + str(self.data)

    def setReceivedMsg(self, data, status):
        self.access_control = self.access_control_not_token
        self.msg_control = self.msg_control
        self.status = status
        self.data = data

        return str(self.start) + str(self.access_control) + str(self.msg_control) + str(self.status) + str(self.sender) + str(self.receiver) + str(self.data)

    def setArrivedMsg(self, data):
        self.access_control = self.access_control_not_token
        self.msg_control = self.msg_control_notification
        receiver = self.sender
        self.sender = self.receiver
        self.receiver = receiver
        self.status = self.status_arrived
        self.data = "Notificacao: " + str(data)

        return str(self.start) + str(self.access_control) + str(self.msg_control) + str(self.status) + str(self.sender) + str(self.receiver) + str(self.data)

    def setTokenMsg(self, sender, receiver):
        self.access_control = self.access_control_is_token
        self.msg_control = 'Z'
        self.data = "Voce esta recebendo o bastao!\n"
        self.sender = sender
        self.receiver = receiver
        self.status = self.status_sent
        return str(self.start) + str(self.access_control) + str(self.msg_control) + str(self.status) + str(self.sender) + str(self.receiver) + str(self.data)

    def attackPos(self):
        i = self.data.find('MOVE:(')
        i+=6
        pos_str = ''
        while self.data[i] != ')':
            pos_str += self.data[i]
            if self.data[i+1] == ',':
                x = pos_str
                pos_str = ''
                i+=1
            i+=1
        y = pos_str
        return '(' + x + ',' + y + ')'
