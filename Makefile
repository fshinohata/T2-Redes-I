# Folders
_LATEX = relatorio

# Programs
LATEX_MK = latexmk -pdf
LIST_FILES = ls
FILTER = grep -P '.*\.(?!pdf$$|tex$$|md$$)'
CLEAN_TEX = xargs rm

# Configuration variables
DOCS = relatorio
LATEX_DOCS = $(foreach latex, $(DOCS), $(_LATEX)/$(latex).tex)


# Default rule
all: doc

# Compiles documents
doc:
	$(LATEX_MK) $(LATEX_DOCS)
	$(LIST_FILES) | $(FILTER) | $(CLEAN_TEX)

clean:
	rm *.pdf

purge: clean