# Trabalho II -- Redes I

Batalha Naval em 4 jogadores, implementado com a rede em anel *Token Ring*.

Implementação feita em *Python*

## Autores

Fernando Aoyagui Shinohata -- GRR20165388  
Lucas Sampaio Franco -- GRR20166836

## Makefile

Como não há necessidade de compilação, o `Makefile` contém uma única regra que compila somente o relatório em formato `.pdf`. É necessário ter o programa `latexmk` instalado.

## O jogo

Os arquivos `playerX.py` contém os executáveis de cada jogador. É necessário pré-configurar os *IPs* em cada um deles, para que funcionem na rede.

A configuração padrão dos *IPs* é para funcionar localmente, em uma única máquina, através de 4 terminais.

## Como jogar

Se for jogar localmente, basta abrir quatro terminais, com cada um executando uma das 4 instâncias de `playerX.py`. **Não execute `game.py` ou `message.py`!**
